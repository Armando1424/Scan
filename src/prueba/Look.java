/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Armando
 */
public class Look extends javax.swing.JFrame {

    /**
     * Creates new form Look
     */
    ArrayList<String> listSoli = new ArrayList<String>();
    ArrayList<String> listTE = new ArrayList<String>();
    ArrayList<String> listDez = new ArrayList<String>();
    ArrayList<String> listTemp = new ArrayList<String>();
    String current;
    DefaultTableModel dtm = new DefaultTableModel();
    public Look() {
        initComponents();
        String[] titulo=new String[]{"Solicitudes","Tiempo de espera","Desplazamiento"};
        dtm.setColumnIdentifiers(titulo);
        tbDatos.setModel(dtm);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnAgregar = new javax.swing.JButton();
        txtCiliSoli = new javax.swing.JTextField();
        lblMostrarCili = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnCE = new javax.swing.JButton();
        btnCalcular = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        txtNumCili = new javax.swing.JTextField();
        txtCabezal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtTiempoPromEs = new javax.swing.JTextField();
        txtTiempoPosTo = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbDatos = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Numero  de Cilindros:");

        jLabel2.setText("Solicitudes");

        btnAgregar.setText("Agregar");
        btnAgregar.setName("btnAgregar"); // NOI18N
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        txtCiliSoli.setName("txtCiliSoli"); // NOI18N

        lblMostrarCili.setName("lblMostrarCili"); // NOI18N

        jLabel3.setText("Posicion inicial del cabezal:");

        btnCE.setText("CE");
        btnCE.setEnabled(false);
        btnCE.setName("btnCE"); // NOI18N
        btnCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCEActionPerformed(evt);
            }
        });

        btnCalcular.setText("Calcular");
        btnCalcular.setName("btnCalcular"); // NOI18N
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.setName("btnLimpiar"); // NOI18N
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        txtNumCili.setName("txtNumCili"); // NOI18N

        txtCabezal.setName("txtCabezal"); // NOI18N

        jLabel4.setText("Tiempo promedio de espera:");

        jLabel5.setText("Un posicionamiento requiere: 6ms");

        jLabel6.setText("Tiempo de posicionamiento total:");

        txtTiempoPromEs.setEditable(false);
        txtTiempoPromEs.setName("txtTiempoPromEs"); // NOI18N

        txtTiempoPosTo.setEditable(false);
        txtTiempoPosTo.setName("txtTiempoPosTo"); // NOI18N

        tbDatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Solicitudes", "Tiempo de espera", "Desplazamiento"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbDatos.setName("tbDatos"); // NOI18N
        jScrollPane1.setViewportView(tbDatos);
        if (tbDatos.getColumnModel().getColumnCount() > 0) {
            tbDatos.getColumnModel().getColumn(0).setResizable(false);
            tbDatos.getColumnModel().getColumn(1).setResizable(false);
            tbDatos.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtNumCili, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(txtCabezal, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40)
                .addComponent(jLabel4)
                .addGap(54, 54, 54)
                .addComponent(txtTiempoPromEs, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel2)
                .addGap(28, 28, 28)
                .addComponent(btnAgregar)
                .addGap(26, 26, 26)
                .addComponent(btnCE)
                .addGap(53, 53, 53)
                .addComponent(jLabel5))
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(txtCiliSoli, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(btnCalcular)
                .addGap(27, 27, 27)
                .addComponent(btnLimpiar)
                .addGap(32, 32, 32)
                .addComponent(jLabel6)
                .addGap(35, 35, 35)
                .addComponent(txtTiempoPosTo, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(lblMostrarCili, javax.swing.GroupLayout.PREFERRED_SIZE, 680, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel1)
                        .addGap(3, 3, 3)
                        .addComponent(txtNumCili, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(5, 5, 5)
                        .addComponent(txtCabezal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(txtTiempoPromEs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(btnAgregar))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(btnCE))
                    .addComponent(jLabel5))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCiliSoli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtTiempoPosTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnCalcular)
                            .addComponent(btnLimpiar))))
                .addGap(25, 25, 25)
                .addComponent(lblMostrarCili, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        // TODO add your handling code here:
        try
        {
            int veamos=Integer.parseInt(txtCiliSoli.getText());
            if (!txtCiliSoli.getText().equals("")) {
                lblMostrarCili.setText(lblMostrarCili.getText()+"  "+txtCiliSoli.getText());
                current=txtCiliSoli.getText();
                if (listTemp.size()>=1) 
                {
                    int num=listTemp.size();
                    for (int i = 0; i < num; i++) 
                    {
                        if (veamos<Integer.parseInt(listTemp.get(i))) 
                        {
                            listTemp.add(listTemp.get(listTemp.size()-1));
                            for (int j = listTemp.size()-2; j > i; j--) 
                            {
                               listTemp.set(j, listTemp.get(j-1));
                            }
                            listTemp.set(i, txtCiliSoli.getText());
                            break;
                        }
                        else if (i==listTemp.size()-1) 
                        {
                            listTemp.add(txtCiliSoli.getText());
                        }
                    }
                }
                else
                {
                    listTemp.add(txtCiliSoli.getText());
                }
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Solo numeros");
        }
        txtCiliSoli.setText("");
        txtCiliSoli.requestFocus();
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
        // TODO add your handling code here:
        try
        {
            int noSeOcupa=Integer.parseInt(txtCabezal.getText());
            String noSeOcupa2=listTemp.get(0);
            proceso();        
            int sumTPE=0,sumTDT=0,size=listTemp.size();        
            for (int i = 0; i < size; i++) 
            {
                dtm.addRow(new Object[]{
                    listTemp.get(i),listTE.get(i),listDez.get(i)
                });
                sumTPE+=Integer.parseInt(listTE.get(i));
                sumTDT+=Integer.parseInt(listDez.get(i));
            }
            double yo=(double)sumTPE/(double)(size);
            txtTiempoPromEs.setText(""+yo);
            txtTiempoPosTo.setText(""+(sumTDT*6)+" ms");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Solo numeros y se necesitan solicitudes");
        }
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCEActionPerformed
        // TODO add your handling code here:
        if(!listTemp.isEmpty())
        {
            listTemp.remove(buscar(current));
            String yo="";
            for (int i = 0; i < listTemp.size(); i++) 
            {
                yo+=listTemp.get(i)+"  ";            
            }
            lblMostrarCili.setText(yo);
        }
    }//GEN-LAST:event_btnCEActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        Clear_Table();
        listSoli.clear();
        listTE.clear();
        listDez.clear();
        listTemp.clear();
        lblMostrarCili.setText("");
        txtCabezal.setText("");
        txtCiliSoli.setText("");
        txtTiempoPromEs.setText("");
        txtTiempoPosTo.setText("");       
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void proceso()
    {
        acomodar();
        calcular();
    }
    
    private void calcular()
    {
        listTE.add("0");
        listDez.add(""+(Integer.parseInt(txtCabezal.getText()) - Integer.parseInt(listSoli.get(0))));
        for (int i = 0; i < listSoli.size()-1; i++) 
        {
            int calc0=(Integer.parseInt(listSoli.get(i)) - Integer.parseInt(listSoli.get(i+1)));
            listDez.add(""+Math.abs(calc0));
            int calc1=Integer.parseInt(listTE.get(i)) + Integer.parseInt(listDez.get(i));
            listTE.add(""+Math.abs(calc1));
        }       
    }
    
    private void acomodar()
    {
        int cont=0;
        int pic=Integer.parseInt(txtCabezal.getText());
        for (int i = 0; i < listTemp.size(); i++) 
        {
            if (pic<Integer.parseInt(listTemp.get(i))) 
            {
               cont=i-1;
               break;
            }
        }
        
        for (int i = cont; i > -1; i--) 
        {
           listSoli.add(listTemp.get(i));
        }
        for (int i = cont+1; i < listTemp.size(); i++) 
        {
            listSoli.add(listTemp.get(i));
        }
        
        for (int i = 0; i < listTemp.size(); i++) 
        {
            int ok=Integer.parseInt(listSoli.get(i));
            listTemp.set(i,""+Math.abs(ok));
        }
    }
    
    private int buscar(String dato)
    {
        int num=-1;
        for (int i = 0; i < listTemp.size(); i++) 
        {
            if (listTemp.get(i).equals(dato)) 
            {
                num=i;
                break;
            }
        }
        return num;
    }
    
    private void Clear_Table()
    {
        if(tbDatos.getRowCount() != 0 )
        {
            while (dtm.getRowCount() > 0) 
            {
                dtm.removeRow(0);
            }
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Look.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Look.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Look.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Look.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Look().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnCE;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblMostrarCili;
    private javax.swing.JTable tbDatos;
    private javax.swing.JTextField txtCabezal;
    private javax.swing.JTextField txtCiliSoli;
    private javax.swing.JTextField txtNumCili;
    private javax.swing.JTextField txtTiempoPosTo;
    private javax.swing.JTextField txtTiempoPromEs;
    // End of variables declaration//GEN-END:variables
}
